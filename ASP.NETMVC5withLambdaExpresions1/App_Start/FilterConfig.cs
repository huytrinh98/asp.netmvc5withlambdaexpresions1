﻿using System.Web;
using System.Web.Mvc;

namespace ASP.NETMVC5withLambdaExpresions1
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
