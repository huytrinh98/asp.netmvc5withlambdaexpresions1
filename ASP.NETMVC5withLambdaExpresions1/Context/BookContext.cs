﻿using ASP.NETMVC5withLambdaExpresions1.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ASP.NETMVC5withLambdaExpresions1.Context
{
    public class BookContext : DbContext
    {
        public DbSet<Book> Books { get; set; }

    }
}